import "classes/*.pp"
import "definitions/*.pp"

node default {
  include sudo
}

node stentyst {
  # Manual configuration:
  #
  # PostgresQL: set superadmin password:
  #
  # sudo su postgres -c psql
  # ALTER USER postgres WITH PASSWORD 'password';
  # \q

  include base
  include puppet
  include puppetclient
  include firewall
  include router_with_dhcp
  include dhcpserver
  include bind9
  include slimserver
  include ntpdmaster
  include django
  include p2p
  include hplip

  dyndns { "ceder.homelinux.com":
    interface => "eth0",
    login     => "ceder",
    passwordfile => "/etc/puppet/passwords/dyndns-ceder",
  }

  mountpoint { "/mirror": label=> "stentyst-mirror" }
  mountpoint { "/music":  label=> "stentyst-music" }
  mountpoint { "/photos": label=> "stentyst-photos" }
  mountpoint { "/usr/local": label=> "stentyst-local" }
  mountpoint { "/movies":  label=> "stentyst-movies" }
  mountpoint { "/cds":  label=> "stentyst-cds" }
  mountpoint { "/gratia-restore":  label=> "stentyst-gratia" }

  bindfile { "named.conf.local": }
  bindfile { "cederqvist.zone": }
  bindfile { "192.168.10.zone": }

  file { "/etc/hosts":
    source => "puppet:///dist/etc/hosts",
    owner => root,
    group => root,
    mode => 444,
  }

  apachemod { "userdir": }

  package { ["squid"]:
    ensure => latest,
  }
}

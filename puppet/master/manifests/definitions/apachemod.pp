define apachemod {
  file { "/etc/apache2/mods-enabled/${name}.load":
    ensure => "../mods-available/${name}.load",
    notify => Service["apache2"],
  }

  file { "/etc/apache2/mods-enabled/${name}.conf":
    ensure => "../mods-available/${name}.conf",
    notify => Service["apache2"],
  }
}

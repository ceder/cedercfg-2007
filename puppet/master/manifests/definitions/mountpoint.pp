define mountpoint($label) {
  file { "$title":
    ensure => directory,
  }

  mount { "$title":
    atboot  => true,
    device  => "LABEL=$label",
    ensure  => mounted,
    fstype  => ext3,
    options => "defaults",
    dump    => "0",
    pass    => "3",
    require => File["$title"],
  }
}


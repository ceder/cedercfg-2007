define bindfile {
  file { "/etc/bind/${name}":
    source => "puppet:///host/etc/bind/${name}",
    owner => root,
    group => bind,
    mode => 644,
    notify => Service["bind9"],
  }
}

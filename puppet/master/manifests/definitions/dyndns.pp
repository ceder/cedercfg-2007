define dyndns($interface, $login, $passwordfile) {
  package { ddclient:
    ensure => latest,
  }

  package { "libio-socket-ssl-perl":
    ensure => latest,
  }

  file { "/etc/default/ddclient":
    source => "puppet:///dist/ddclient/default",
  }

  file { "/etc/ddclient.conf":
    content => template("ddclient.erb"),
    owner => root,
    group => root,
    mode => 400,
  }

  service { ddclient:
    subscribe => [Package["ddclient"], 
                  Package["libio-socket-ssl-perl"],
                  File["/etc/default/ddclient"],
                  File["/etc/ddclient.conf"]],
    enable    => true,
    ensure    => running,
    hasstatus => true,
  }
}

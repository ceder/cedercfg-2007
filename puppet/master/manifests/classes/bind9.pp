class bind9 {
  package { "bind9":
    ensure => latest,
    notify => Service[bind9],
  }

  service { "bind9":
    enable    => true,
  }
}

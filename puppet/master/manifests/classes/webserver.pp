class apache {
  package { "apache2":
    ensure => latest,
  }

  service { "apache2":
    enable    => true,
    ensure    => running,
    subscribe => Package["apache2"],
  }
}

class postgresql {
  package { ["python-psycopg2", "postgresql"]:
    ensure => latest,
    notify => Service["apache2"],
  }
}

class django {
  include apache
  include postgresql

  package { ["python-django", "libapache2-mod-python"]:
    ensure => latest,
    notify => Service["apache2"],
  }
}

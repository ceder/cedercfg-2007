class dhcpserver {
  package { "dhcp3-server":
    ensure => latest,
    notify => Service["dhcp3-server"],
  }

  file { "/etc/default/dhcp3-server":
    source => "puppet:///host/etc/default/dhcp3-server",
    owner => root,
    group => root,
    mode => 444,
    notify => Service["dhcp3-server"],
  }

  file { "/etc/dhcp3/dhcpd.conf":
    source => "puppet:///host/etc/dhcp3/dhcpd.conf",
    owner => root,
    group => root,
    mode => 444,
    notify => Service["dhcp3-server"],
  }

  service { "dhcp3-server":
    ensure    => running,
    enable    => true,
    hasstatus => true,
  }
}

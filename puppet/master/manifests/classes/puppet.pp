class puppet {
  file { "/etc/puppet/puppetmasterd.conf":
    owner  => root,
    group  => root,
    mode   => 644,
    source => "puppet:///dist/puppet/puppetmasterd.conf",
  }

  package { "puppetmaster":
    ensure => latest;
  }

  service { "puppetmaster":
    subscribe => [File["/etc/puppet/puppetmasterd.conf"],
                  Package["puppetmaster"]],
    enable    => true,
    ensure    => running,
  }
}

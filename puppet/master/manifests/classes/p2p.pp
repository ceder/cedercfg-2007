class p2p {
  package { "azureus":
    ensure => latest,
  }

  package { ["transmission"]:
    ensure => latest,
  }
}

class firewall {
  file { "/etc/init.d/firewall":
    source => "puppet:///host/etc/init.d/firewall",
    owner => root,
    group => root,
    mode => 555,
    notify => Service[firewall],
  }

  service { firewall:
    ensure    => running,
    enable    => true,
    hasstatus => true,
  }
}

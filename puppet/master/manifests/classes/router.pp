class router_with_dhcp {
  file { "/etc/network/interfaces":
    source => "puppet:///host/etc/network/interfaces",
    notify => Service[networking],
  }

  file { "/etc/dhcp3/dhclient.conf":
    source => "puppet:///host/etc/dhcp3/dhclient.conf",
    notify => Service[networking],
  }

  service { networking:
    enable    => true,
  }
}

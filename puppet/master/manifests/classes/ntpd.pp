class ntpd {
  package { ["ntp", "ntp-doc"]:
    ensure => latest,
  }

  service { ntp:
    subscribe => [Package["ntp"],
                  File["/etc/ntp.conf"]],
    enable    => true,
    ensure    => running,
    hasstatus => true,
  }
}

class ntpdmaster {
  file { "/etc/ntp.conf":
    source => "puppet:///dist/etc/ntp.conf.server",
    owner => root,
    group => root,
    mode => 444,
  }
}

class ntpdclient inherits ntpd {
  file { "/etc/ntp.conf":
    source => "puppet:///dist/etc/ntp.conf.client",
    owner => root,
    group => root,
    mode => 444,
  }
}

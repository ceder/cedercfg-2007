class base {
  package { "debconf-doc":
    ensure => latest,
  }

  package { "python-beautifulsoup":
    ensure => latest,
  }

  package { "xloadimage":
    ensure => latest,
  }

  package { "mplayer":
    ensure => latest,
  }

  package { "build-essential":
    ensure => latest,
  }

  package { ["texinfo", "texinfo-doc-nonfree"]:
    ensure => latest,
  }

  package { "dejagnu":
    ensure => latest,
  }

  package { ["tetex-bin", "tetex-doc-nonfree", "tetex-src", "texlive-doc-en",
    "texlive-fonts-extra", "texlive-font-utils", "texlive-lang-swedish"]:
    ensure => latest,
  }

  package { ["emacs"]:
    ensure => latest,
  }

  package { ["openssh-server"]:
    ensure => latest,
  }

  package { ["miro", "libxine1-ffmpeg", "python-psyco"]:
    ensure => latest,
  }

  package { ["flashplugin-nonfree", "msttcorefonts", "ttf-xfree86-nonfree",
             "xfs"]:
    ensure => latest,
  }

  package { ["cdrecord", "mkisofs", "cdda2wav"]:
    ensure => latest,
  }

  package { ["python-id3", "eyed3", "python-eyed3", "python-mutagen"]:
    ensure => latest,
  }

  package { ["automake", "python-dev", "valgrind", "make-doc"]:
    ensure => latest,
  }

  package { ["sane-utils"]:
    ensure => latest,
  }

  package { ["mercurial", "rcs", "cvs", "hgsvn"]:
    ensure => latest,
  }

  package { ["xpdf-reader", "xpdf-utils"]:
    ensure => latest,
  }

  package { ["elinks"]:
    ensure => latest,
  }

  package { ["recode"]:
    ensure => latest,
  }

  package { ["diffstat"]:
    ensure => latest,
  }

  package { ["python-matplotlib", "python-matplotlib-doc", "dvipng" ]:
    ensure => latest,
  }

  package { ["postfix", "postfix-doc", "mailutils"]:
    ensure => latest,
  }

  package { ["wireshark"]:
    ensure => latest,
  }

  package { ["plucker", "imagemagick", "libjpeg-progs", "pilot-link"]:
    ensure => latest,
  }

  package { ["unrar"]:
    ensure => latest,
  }

  package { ["enscript"]:
    ensure => latest,
  }

  package { ["apt-doc", "dctrl-tools"]:
    ensure => latest,
  }
}

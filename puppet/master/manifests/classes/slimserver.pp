class slimserver {
  package { "slimserver":
    ensure => latest,
    notify => Service["slimserver"],
  }

  package { ["lame", "lame-extras"]:
    ensure => latest,
    notify => Service["slimserver"],
  }

  file { "/etc/default/slimserver":
    source => "puppet:///host/etc/default/slimserver",
    owner => root,
    group => root,
    mode => 444,
    notify => Service["slimserver"],
  }

  service { "slimserver":
    ensure    => running,
    enable    => true,
    require   => Mount["/music"],
    subscribe => File["/etc/hosts"],
  }
}

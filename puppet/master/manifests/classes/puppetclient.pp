class puppetclient {
  file { "/etc/puppet/puppetd.conf":
    owner  => root,
    group  => root,
    mode   => 644,
    source => "puppet:///dist/puppet/puppetd.conf",
  }
  
  package { "puppet":
    ensure => latest;
  }

  service { "puppet":
    subscribe => [File["/etc/puppet/puppetd.conf"],
                  Package["puppet"]],
    enable    => true,
    ensure    => running,
  }
}
